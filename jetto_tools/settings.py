"""
This setting.py file contains code copied from Matplotlib
v3.3.0rc1-624-g2855691ee. Matplotlib is released under the
Matplotlib license, which is based on the PSF license. Under
the terms of this license, we copy here source code slightly modified
to be runnable on this devs PC. This code stays owned by the Matplotlib
dev team, under the following statement:

Copyright (c) 2012-2013 Matplotlib Development Team; All Rights Reserved

And any changes made to this file should stay open and free.

If this was useful for you, consider
[citing matplotlib](https://matplotlib.org/citing.html)
and donating money and/or time to the [numfocus](https://numfocus.org/)
project
"""
import atexit
from collections import namedtuple
from collections.abc import MutableMapping
import contextlib
from distutils.version import LooseVersion
import functools
import importlib
import inspect
from inspect import Parameter
import locale
import logging
import os
from pathlib import Path
import pprint
import re
import shutil
import subprocess
import sys
import tempfile
import warnings

from IPython import embed

_log = logging.getLogger(__name__)


def get_configdir():
    configdir = Path.home() / '.config'
    return configdir


def settings_fname():
    """
    Get the location of the config file.

    The file location is determined in the following order

    - ``$PWD/jetto_pythontoolsrc``
    - ``$HOME/.config/jetto_pythontoolsrc``. If it doesn't exist, it is created
    """

    def gen_candidates():
        yield Path.cwd() / 'jetto_pythontoolsrc'
        yield get_configdir() / 'jetto_pythontoolsrc'

    for file in gen_candidates():
        if file.exists() and not file.is_dir():
            return file

    # jetto_pythontoolsrc does not exist, create it
    with file.open('w') as f_:
        f_.write('{')
        f_.write('}')
        f_.write('# vi: ft=json')


def validate_bool(b):
    """Convert b to ``bool`` or raise."""
    if isinstance(b, str):
        b = b.lower()
    if b in ('t', 'y', 'yes', 'on', 'true', '1', 1, True):
        return True
    elif b in ('f', 'n', 'no', 'off', 'false', '0', 0, False):
        return False
    else:
        raise ValueError('Could not convert "%s" to bool' % b)

class RcParams(MutableMapping, dict):
    """
    A dictionary object including validation.

    """

    validate = {
    }

    # validate values on the way in
    def __init__(self, *args, **kwargs):
        self.update(*args, **kwargs)

    def __setitem__(self, key, val):
        try:
            try:
                cval = self.validate[key](val)
            except ValueError as ve:
                raise ValueError(f"Key {key}: {ve}") from None
            dict.__setitem__(self, key, cval)
        except KeyError as err:
            raise KeyError(
                f"{key} is not a valid rc parameter (see rcParams.keys() for "
                f"a list of valid parameters)") from err

    def __getitem__(self, key):
        return dict.__getitem__(self, key)

    def __repr__(self):
        class_name = self.__class__.__name__
        indent = len(class_name) + 1
        repr_split = pprint.pformat(dict(self), indent=1,
                                    width=80 - indent).split('\n')
        repr_indented = ('\n' + ' ' * indent).join(repr_split)
        return '{}({})'.format(class_name, repr_indented)

    def __str__(self):
        return '\n'.join(map('{0[0]}: {0[1]}'.format, sorted(self.items())))

    def __iter__(self):
        """Yield sorted list of keys."""
        yield from sorted(dict.__iter__(self))

    def __len__(self):
        return dict.__len__(self)

    def find_all(self, pattern):
        """
        Return the subset of this RcParams dictionary whose keys match,
        using :func:`re.search`, the given ``pattern``.

        .. note::

            Changes to the returned dictionary are *not* propagated to
            the parent RcParams dictionary.

        """
        pattern_re = re.compile(pattern)
        return RcParams((key, value)
                        for key, value in self.items()
                        if pattern_re.search(key))

    def copy(self):
        return {k: dict.__getitem__(self, k) for k in self}


def rc_params(fail_on_error=False):
    """Construct a `RcParams` instance from the default Matplotlib rc file."""
    return rc_params_from_file(settings_fname(), fail_on_error)


def is_url(filename):
    """Return True if string is an http, ftp, or file URL path."""
    return URL_REGEX.match(filename) is not None


@contextlib.contextmanager
def _open_file_or_url(fname):
    if not isinstance(fname, Path) and is_url(fname):
        import urllib.request
        with urllib.request.urlopen(fname) as f:
            yield (line.decode('utf-8') for line in f)
    else:
        fname = os.path.expanduser(fname)
        encoding = locale.getpreferredencoding(do_setlocale=False)
        if encoding is None:
            encoding = "utf-8"
        with open(fname, encoding=encoding) as f:
            yield f


def _rc_params_in_file(fname, transform=lambda x: x, fail_on_error=False):
    """
    Construct a `RcParams` instance from file *fname*.

    Unlike `rc_params_from_file`, the configuration class only contains the
    parameters specified in the file (i.e. default values are not filled in).

    Parameters
    ----------
    fname : path-like
        The loaded file.
    transform : callable, default: the identity function
        A function called on each individual line of the file to transform it,
        before further parsing.
    fail_on_error : bool, default: False
        Whether invalid entries should result in an exception or a warning.
    """
    rc_temp = {}
    with _open_file_or_url(fname) as fd:
        try:
            for line_no, line in enumerate(fd, 1):
                line = transform(line)
                strippedline = line.split('#', 1)[0].strip()
                if not strippedline:
                    continue
                tup = strippedline.split(':', 1)
                if len(tup) != 2:
                    _log.warning('Missing colon in file %r, line %d (%r)',
                                 fname, line_no, line.rstrip('\n'))
                    continue
                key, val = tup
                key = key.strip()
                val = val.strip()
                if key in rc_temp:
                    _log.warning('Duplicate key in file %r, line %d (%r)',
                                 fname, line_no, line.rstrip('\n'))
                rc_temp[key] = (val, line, line_no)
        except UnicodeDecodeError:
            _log.warning('Cannot decode configuration file %s with encoding '
                         '%s, check LANG and LC_* variables.',
                         fname,
                         locale.getpreferredencoding(do_setlocale=False)
                         or 'utf-8 (default)')
            raise

    config = RcParams()

    for key, (val, line, line_no) in rc_temp.items():
        if key in rcsetup._validators:
            if fail_on_error:
                config[key] = val  # try to convert to proper type or raise
            else:
                try:
                    config[key] = val  # try to convert to proper type or skip
                except Exception as msg:
                    _log.warning('Bad value in file %r, line %d (%r): %s',
                                 fname, line_no, line.rstrip('\n'), msg)
        elif key in _deprecated_ignore_map:
            version, alt_key = _deprecated_ignore_map[key]
            cbook.warn_deprecated(
                version, name=key, alternative=alt_key,
                addendum="Please update your matplotlibrc.")
        else:
            version = 'master' if '.post' in __version__ else f'v{__version__}'
            _log.warning("""
Bad key %(key)s in file %(fname)s, line %(line_no)s (%(line)r)
You probably need to get an updated matplotlibrc file from
https://github.com/matplotlib/matplotlib/blob/%(version)s/matplotlibrc.template
or from the matplotlib source distribution""",
                         dict(key=key, fname=fname, line_no=line_no,
                              line=line.rstrip('\n'), version=version))
    return config


def rc_params_from_file(fname, fail_on_error=False, use_default_template=True):
    """
    Construct a `RcParams` from file *fname*.

    Parameters
    ----------
    fname : str or path-like
        A file with Matplotlib rc settings.
    fail_on_error : bool
        If True, raise an error when the parser fails to convert a parameter.
    use_default_template : bool
        If True, initialize with default parameters before updating with those
        in the given file. If False, the configuration class only contains the
        parameters specified in the file. (Useful for updating dicts.)
    """
    config_from_file = _rc_params_in_file(fname, fail_on_error=fail_on_error)

    if not use_default_template:
        return config_from_file

    with cbook._suppress_matplotlib_deprecation_warning():
        config = RcParams({**rcParamsDefault, **config_from_file})

    with cbook._suppress_matplotlib_deprecation_warning():
        if config['datapath'] is None:
            config['datapath'] = _get_data_path()
        else:
            config['datapath'] = get_data_path(_from_rc=config['datapath'])

    if "".join(config['text.latex.preamble']):
        _log.info("""
*****************************************************************
You have the following UNSUPPORTED LaTeX preamble customizations:
%s
Please do not ask for support with these customizations active.
*****************************************************************
""", '\n'.join(config['text.latex.preamble']))
    _log.debug('loaded rc file %s', fname)

    return config




def rc(group, **kwargs):
    """
    Set the current `.rcParams`.  *group* is the grouping for the rc, e.g.,
    for ``lines.linewidth`` the group is ``lines``, for
    ``axes.facecolor``, the group is ``axes``, and so on.  Group may
    also be a list or tuple of group names, e.g., (*xtick*, *ytick*).
    *kwargs* is a dictionary attribute name/value pairs, e.g.,::

      rc('lines', linewidth=2, color='r')

    sets the current `.rcParams` and is equivalent to::

      rcParams['lines.linewidth'] = 2
      rcParams['lines.color'] = 'r'

    The following aliases are available to save typing for interactive users:

    =====   =================
    Alias   Property
    =====   =================
    'lw'    'linewidth'
    'ls'    'linestyle'
    'c'     'color'
    'fc'    'facecolor'
    'ec'    'edgecolor'
    'mew'   'markeredgewidth'
    'aa'    'antialiased'
    =====   =================

    Thus you could abbreviate the above call as::

          rc('lines', lw=2, c='r')

    Note you can use python's kwargs dictionary facility to store
    dictionaries of default parameters.  e.g., you can customize the
    font rc as follows::

      font = {'family' : 'monospace',
              'weight' : 'bold',
              'size'   : 'larger'}
      rc('font', **font)  # pass in the font dict as kwargs

    This enables you to easily switch between several configurations.  Use
    ``matplotlib.style.use('default')`` or :func:`~matplotlib.rcdefaults` to
    restore the default `.rcParams` after changes.

    Notes
    -----
    Similar functionality is available by using the normal dict interface, i.e.
    ``rcParams.update({"lines.linewidth": 2, ...})`` (but ``rcParams.update``
    does not support abbreviations or grouping).
    """

    aliases = {
        'lw':  'linewidth',
        'ls':  'linestyle',
        'c':   'color',
        'fc':  'facecolor',
        'ec':  'edgecolor',
        'mew': 'markeredgewidth',
        'aa':  'antialiased',
        }

    if isinstance(group, str):
        group = (group,)
    for g in group:
        for k, v in kwargs.items():
            name = aliases.get(k) or k
            key = '%s.%s' % (g, name)
            try:
                rcParams[key] = v
            except KeyError as err:
                raise KeyError(('Unrecognized key "%s" for group "%s" and '
                                'name "%s"') % (key, g, name)) from err


def rcdefaults():
    """
    Restore the `.rcParams` from Matplotlib's internal default style.

    Style-blacklisted `.rcParams` (defined in
    `matplotlib.style.core.STYLE_BLACKLIST`) are not updated.

    See Also
    --------
    matplotlib.rc_file_defaults
        Restore the `.rcParams` from the rc file originally loaded by
        Matplotlib.
    matplotlib.style.use
        Use a specific style file.  Call ``style.use('default')`` to restore
        the default style.
    """
    # Deprecation warnings were already handled when creating rcParamsDefault,
    # no need to reemit them here.
    with cbook._suppress_matplotlib_deprecation_warning():
        from .style.core import STYLE_BLACKLIST
        rcParams.clear()
        rcParams.update({k: v for k, v in rcParamsDefault.items()
                         if k not in STYLE_BLACKLIST})


def rc_file_defaults():
    """
    Restore the `.rcParams` from the original rc file loaded by Matplotlib.

    Style-blacklisted `.rcParams` (defined in
    `matplotlib.style.core.STYLE_BLACKLIST`) are not updated.
    """
    # Deprecation warnings were already handled when creating rcParamsOrig, no
    # need to reemit them here.
    with cbook._suppress_matplotlib_deprecation_warning():
        from .style.core import STYLE_BLACKLIST
        rcParams.update({k: rcParamsOrig[k] for k in rcParamsOrig
                         if k not in STYLE_BLACKLIST})


def rc_file(fname, *, use_default_template=True):
    """
    Update `.rcParams` from file.

    Style-blacklisted `.rcParams` (defined in
    `matplotlib.style.core.STYLE_BLACKLIST`) are not updated.

    Parameters
    ----------
    fname : str or path-like
        A file with Matplotlib rc settings.

    use_default_template : bool
        If True, initialize with default parameters before updating with those
        in the given file. If False, the current configuration persists
        and only the parameters specified in the file are updated.
    """
    # Deprecation warnings were already handled in rc_params_from_file, no need
    # to reemit them here.
    with cbook._suppress_matplotlib_deprecation_warning():
        from .style.core import STYLE_BLACKLIST
        rc_from_file = rc_params_from_file(
            fname, use_default_template=use_default_template)
        rcParams.update({k: rc_from_file[k] for k in rc_from_file
                         if k not in STYLE_BLACKLIST})


@contextlib.contextmanager
def rc_context(rc=None, fname=None):
    """
    Return a context manager for temporarily changing rcParams.

    Parameters
    ----------
    rc : dict
        The rcParams to temporarily set.
    fname : str or path-like
        A file with Matplotlib rc settings. If both *fname* and *rc* are given,
        settings from *rc* take precedence.

    See Also
    --------
    :ref:`customizing-with-matplotlibrc-files`

    Examples
    --------
    Passing explicit values via a dict::

        with mpl.rc_context({'interactive': False}):
            fig, ax = plt.subplots()
            ax.plot(range(3), range(3))
            fig.savefig('example.png')
            plt.close(fig)

    Loading settings from a file::

         with mpl.rc_context(fname='print.rc'):
             plt.plot(x, y)  # uses 'print.rc'

    """
    orig = rcParams.copy()
    try:
        if fname:
            rc_file(fname)
        if rc:
            rcParams.update(rc)
        yield
    finally:
        dict.update(rcParams, orig)  # Revert to the original rcs.


validators = {
    "jpyplot.debug": validate_bool,
}

#settings_fname()
URL_REGEX = re.compile(r'^http://|^https://|^ftp://|^file:')
#
#
## When constructing the global instances, we need to perform certain updates
## by explicitly calling the superclass (dict.update, dict.items) to avoid
## triggering resolution of _auto_backend_sentinel.
#rcParamsDefault = _rc_params_in_file(
#    cbook._get_data_path("matplotlibrc"),
#    # Strip leading comment.
#    transform=lambda line: line[1:] if line.startswith("#") else line,
#    fail_on_error=True)
#dict.update(rcParamsDefault, rcsetup._hardcoded_defaults)
#rcParams = RcParams()  # The global instance.
#dict.update(rcParams, dict.items(rcParamsDefault))
#dict.update(rcParams, _rc_params_in_file(matplotlib_fname()))
#with cbook._suppress_matplotlib_deprecation_warning():
#    rcParamsOrig = RcParams(rcParams.copy())
#    # This also checks that all rcParams are indeed listed in the template.
#    # Assiging to rcsetup.defaultParams is left only for backcompat.
#    defaultParams = rcsetup.defaultParams = {
#        # We want to resolve deprecated rcParams, but not backend...
#        key: [(rcsetup._auto_backend_sentinel if key == "backend" else
#               rcParamsDefault[key]),
#              validator]
#        for key, validator in rcsetup._validators.items()}
#if rcParams['axes.formatter.use_locale']:
#    locale.setlocale(locale.LC_ALL, '')
