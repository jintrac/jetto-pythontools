*
*File Header
#char;1;1;File Format;0
b
#char;1;1;File Description;0
EX-FILE
#char;1;1;Version;0
1.0
#char;1;1;Date;0
07/08/2018
#char;1;1;Time;0
22:36:03
*
*General Info
#char;1;1;Database Name;0
JET PPF
#char;1;1;User EX-file;0
/u/fcasson/cmg/jams/data/exfile/testdata4.ex
#char;1;1;User Pre-Modex EX-file;0
/u/fcasson/cmg/jams/data/exfile/testdata3.ex
*
*PPF Attributes
#int;1;1;Shot;0
 h�
#char;1;1;DDA Name;0
EX
*
*PPF Base Vectors
#float;1;131;XVEC1;3

RHO normalised
1.0
    ;}�<=Ƭ<�%�<�g�=U=-�=M�;=m8W=�l�=�=H=��=��e=Ů�=��=�P=� �>x�>
`�>I$>1k>"�>*�>1�@>9҈>A��>I�>Q�]>Ys�>a[�>iD3>q,z>y�>�~�>�r�>�f�>�Z�>�O>�C5>�7Y>�+}>��>��>��>��>��/>��R>��v>�̚>���>ô�>ǩ>˝)>ϑK>Ӆo>�y�>�m�>�a�>�U�>�J!>�>D>�2h>�&�>��>��>��?{�?u�?o�?i�?	c�?]�?W�?R
?L?F.?@>?:P?4b?.u?(�?"�?!�?#�?%�?'
�?)�?*�?,�?.�&?0�9?2�J?4�[?6�m?8�?:ϑ?<ɣ?>õ?@��?B��?D��?F��?H�?J�?L�1?N�B?P�T?R�f?T�x?V|�?Xv�?Zp�?\j�?^d�?`^�?bX�?dS?fM?hG+?jA;?l;M?n5_?p/q?r)�?t#�?v�?x�?z�?|�?~�?�  
*
*Profiles
#float;1;1;TVEC1;3
secs
TIME
1.0
B9�
#float;1;131;RA;8

Minor radius, normalised
1.0
XVEC1
JETPPF
EFTF

241
    <�*<�r�<���=	ʲ=+�=M�0=q��=�-I=� X=���=�#�=Ֆ�=��=�v]>oU>�>ؖ>">+Av>4v.>=��>F��>P�>YI�>b~,>k�">t�m>~�>���>�<�>�ӗ>�i�>��w>���>�#�>��x>�Ai>��6>�V�>��|>�a�>��V>�b>���>�V�>�̯>�?]>ٮ�>�&>�>���>�L>�>��>�_>���? ��?�?ό?�y?	�?6x?U�?s$?�$?��?Ħ?��?�?i?)? &�?"4?$??&G�?(MU?*Ps?,P�?.N?0H�?2?�?43�?6$�?8Z?9��?;�D?=Ɔ??�7?A�V?CZ�?E/�?G �?H�?J��?L]K?N ?O�?Q��?SL�?T��?V��?XV�?Y�c?[�?]>�?^�3?`q�?b�?c��?e"n?f�T?h0�?i�?k/�?l��?n�?o��?p�?rhT?s��?u+�?v��?wځ?y))?zp�?{�"?|�O?~ �?	�?�  
#float;1;131;PSI;8

Normalised poloidal flux
1.0
XVEC1
JETPPF
EFTF
x-vec
241
    8�]�9�b8:`p�:��;�m;U��;�`�;�ٰ;���<�<@��<f�/<�_ <��<�)$<��<�P=��=j�=$�y=5�
=G�T=Z<,=m�V=��j=�e�=�>�=�u�=�
M=��=�C�=��,=��
=�)*=�ǿ>�q>
�L>?l>��>!8D>(��>0�k>8��>@�=>H�=>QI�>Y�6>b@>j�>s��>|v8>���>�3�>��>�X�>���>��?>�[=>�:>���>���>���>�d >�I�>�6I>�)�>�#>�"�>�'�>�2>�B>>�V�>�o�>�2>�r>��_>���?�?��?B�?�i?v�?!?�?J�?�?��?"k?��? ]k?"��?%��?(4??*�:?-k?0�?2�Z?57�?7Ε?:d?<��??��?B�?D��?G3?I��?LA�?N�?QE&?S��?V;?X��?["D?]��?_�}?b\�?d��?g�?ihv?k�%?m�?p7O?rl�?t� ?v�
?x�?z̊?|��?~d�?�  
#float;1;131;XRHO;8

Sqrt of normalised toroidal flux
1.0
XVEC1
JETPPF
EFTF

241
    ;}�<=ƭ<�%�<�g�=U=-�=M�;=m8X=�l�=�=H=��=��e=Ů�=��=�P=� �>x�>
`�>I%>1l>"�>*�>1�B>9҉>A��>I�>Q�^>Ys�>a[�>iD4>q,{>y�>�~�>�r�>�f�>�Z�>�O>�C7>�7[>�+~>��>��>��>��>��0>��T>��v>�̛>���>ô�>ǩ>˝(>ϑM>Ӆq>�y�>�m�>�a�>�U�>�J">�>F>�2i>�&�>��>��>��?{�?u�?o�?i�?	c�?]�?W�?R
?L?F.?@@?:Q?4d?.u?(�?"�?!�?#�?%�?'
�?)�?*�?,�?.�(?0�8?2�K?4�\?6�n?8Ձ?:ϒ?<ɣ?>õ?@��?B��?D��?F��?H�?J� ?L�2?N�D?P�V?R�h?T�y?V|�?Xv�?Zp�?\j�?^d�?`^�?bX�?dS?fM?hG+?jA=?l;N?n5a?p/s?r)�?t#�?v�?x�?z�?|�?~�?�  
#float;1;131;SPSI;8

Sqrt of normalised poloidal flux
1.0
XVEC1
JETPPF
EFTF
x-vec
241
    <*8n<�l�<�w=��=B��=i�*=�Y=�T�=��M=��=� 4=�,�>�>��>�>#��>.1>8�k>B�$>MR>W��>b
�>l]O>v��>�u�>���>��T>�>��o>�ߘ>���>��	>���>��>�΋>���>��B>�~!>�Ve>�'�>��[>Ե�>�q�>�&.>��>�x>�C>�{>�7�>���>�9�?W?�[?�?�?
x?=Q?_?|�?�n?�
?��?�B?��?ژ?�_? �??"�>?$�b?&��?(�.?*��?,|�?._�?0?d?2?3�?5�\?7��?9d�?;.?<�?>��?@s�?B.�?C�?E�?GH�?H�%?J��?LB�?M�x?O�w?Q�?R��?TG%?U��?Wc?X�?Zp�?[��?]o�?^�?`_�?a�?c@�?d��?fj?gup?hԉ?j/�?k��?l��?n(�?osM?p��?q��?s8�?tqs?u��?v��?w��?y�?z;�?{R?|`?}b�?~S�?1�?�  
#float;1;131;R;8
m
Major radius
0.01
XVEC1
JETPPF
EFTF

241
C�=C�t�C�ҘC�'>C�|6C�ӶC�.�C���C��vC�QC���C�|C�tkC��dC�6%C���C���C�XQC��
C��C�zC��;C�;�C���C��vC�^/C���C��C��4C���C�AUC���C�.C�buC�C�"�C���C��<C�A�C��C� @C�_,C���C�WC�z�C�؏C�6KC���C���C�M�C���C�C�c(C��C��C�u�C���C�+�C��7C��qC�:gC��C��C�F�C���C��FC�P�C��/C�C�X�C��C��C�]�C���C�	�C�_+C��0C��C�\�C���C��C�VGC��cC���C�KC��|C��fC�:�C���C�׬C�%?C�r7C���C�
RC�UqC���C���C�3 C�{�C��uC�
�C�QMC��FC�ܠC�!]C�e|C��C���C�.:C�o�C��C��C�1wC�p�C��pC��zC�*�C�g�C���C���C�tC�SC���C���C���C�0�C�eC��FC��CC���C�>
#float;1;131;RHO;8
m
JETTO rho coordinate
0.01
XVEC1
JETPPF
EFTF

241
    >뉶?��I@6@N�@�}w@��@�_�@��@�BRA��A�`A)J�A8�AF�2AUt�Ad-iAr�A��PA�+�A���A��9A�@�A���A��"A�UoA���A�A�jYA�ƦA�"�A�CA�ېA�7�A��*A��yB�cBT�B
�B��B^�B%B�LBisB �B#��B's�B+"B.�6B2~]B6,�B9ڪB=��BA6�BD�BH�DBLAlBO�BS��BWK�BZ�	B^�.BbVVBf|Bi��Bm`�Bq�Bt�Bxk?B|dBǌB���B���B�i B�@B�'B��;B��NB��bB�suB�J�B�!�B���B���B���B�}�B�T�B�,B�$B��8B��JB��^B�_qB�6�B��B��B���B���B�i�B�@�B�B�� B��3B��GB�t[B�KnB�"�B���B�ШBǧ�B�~�B�U�B�,�B�	B��BҲ/BԉDB�`XB�7iB�~B��Bݼ�Bߓ�B�j�B�A�B��B��B��B�.B�u?B�LS
#float;1;131;PR;8
Pa
Pressure (from equilibrium)
1.0
XVEC1
JETPPF
EFTF
P
241
_��#_��_���_���_��P_�q�_�Gn_�_��f_���_�L�_��_���_�=:_��b_�b_��_�i�_��M_�U�_���_�'�_���_��S_�3�_���_��Z_��_~�_}>_{uL_y֬_x/�_v�_t�	_s_qJo_o��_m�^_kܴ_j#_h%_fB�_d]_bs�_`��_^��_\��_Z��_X�U_V�w_Tӣ_R�!_P�B_N�D_L�t_J�_I\_G�_E�_C(�_A6�_?F�_=X�_;m_9��_7�r_5��_3�O_1��_0�_.C-_,m�_*��_(�__'m_%;!_#w�_!��_��_C�_��_ߓ_3�_��_��_H*_��_d_�P_�s_h�_
�p_	`G_�\_h�_�4_��_�_ �^���^�ͪ^��^�dN^��^��^�r^��F^�k^��^�rB^��^ߘ�^�7/^��c^؋^�@6^���^��q^ϋi^�]^�6�^��^���^��^��/^��n^��E^��^��c^�	�
#float;1;131;Q;8

q (safety factor)
1.0
XVEC1
JETPPF
EFTF
Q
241
?�h�?�kB?�q�?�{�?���?��U?���?��+?��z?�"?�A�?�sb?��9?��y?�.[?�zq?��W?�*�?��?���?�r�?��I?�u�?��?��?�8�?�߾?��D?�E!?�?���?��=?���?�o�?�g�?�k�?�z�?���?���?��?�2�?���?��5?�B�?���?�:�?���?�a?�q?��#?�r�?�7�?��?��f?��	?ƭa?ȡ�?ʞ4?̣L?α?���?��m?�
?�7�?�mp?۪�?���?�=�?��?��`?�V�?�Ř?�<�??�G�?��o?�x?��?��\?���@ ��@A@�!@�+@v�@�@	��@�@��@@�@�x@��@=J@�@��@��@S�@1O@@ �@"?@$#�@&A�@(m�@*��@,��@/W�@1ʻ@4O
@6�u@9�T@<Xe@?-]@B)�@EI�@H{R@K�>@O� @SN�@WIi@[�X@`��@e�@@j��@qF�@x��@�Ύ@�u�@�..@�0�@�WT
#float;1;131;NE;8
m-3
Electron Density
1000000.0
XVEC1
fcasson
PRFL
NE
559
V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V��V�bV�=V�V��V��V�3V��V�AV��V��V�V~�V}�V{�Vz%Vw�Vu`VrVVn�Vj�Ve�V` VY�VRVIGV?6V3�V&cV7V�V�+VۼV�IV�uV��V`!V6�V=V�V��VXUVzV�YVd.V #V�[V�V��V�.VW�V
��V	�V	
�V!�V$iV�V�V�VOV ��U���U�R�U���U�MU�jU��vU�U��kU�6U�;LU��UμhU�?!UÝ�U��ZU�4U��U��U��U��U�� U�ڹU��|U��FU��Ux��UmX�UbQuUW�UM�UB��
#float;1;131;TE;8
eV
Electron Temperature
1.0
XVEC1
fcasson
PRFL
TE
558
E;� E;OE;y�E;n�E;^E;G�E;,NE;E:�^E:�E:�LE:N�E:E9ϰE9��E9:HE8�FE8��E80�E7�E7c�E6�@E6�E6NE5�	E5:E4x�E3�E3S�E2��E21E1r.E0ƤE0�E/^�E.��E-�#E-�E,M/E+z�E*�E)��E(��E'�vE'�E&
E% E$!zE#fE"�E!�E��EվE� E��Ee�E5�E��E�AE�NE<�E��E�AEH,E�E�gE!�E��E
A�E�wEK�E�KE?hE��ED�D��D��D�+�D�̅D�b6D���D�lcD���D�JLDܨ�D���D�D(DсQDͳgD��hD��]D�AD�D��D��~D��D���D��#D�G�D���D��,D�O`D��D�r�D��D�i�D��pDzh|Dq�Dg�ND^"�DT��DJڋDAZD7:	D-H�D#@�D#8D�fD�dC�Cߝ�Cʅ,C�@C�ξC�1ChΠC<�OC�hB�  
#float;1;131;TI;8
eV
Ion Temperature
1.0
XVEC1
fcasson
PRFL
TI
734
E;� E;OE;y�E;n�E;^E;G�E;,NE;E:�^E:�E:�LE:N�E:E9ϰE9��E9:HE8�FE8��E80�E7�E7c�E6�@E6�E6NE5�	E5:E4x�E3�E3S�E2��E21E1r.E0ƤE0�E/^�E.��E-�#E-�E,M/E+z�E*�E)��E(��E'�vE'�E&
E% E$!zE#fE"�E!�E��EվE� E��Ee�E5�E��E�AE�NE<�E��E�AEH,E�E�gE!�E��E
A�E�wEK�E�KE?hE��ED�D��D��D�+�D�̅D�b6D���D�lcD���D�JLDܨ�D���D�D(DсQDͳgD��hD��]D�AD�D��D��~D��D���D��#D�G�D���D��,D�O`D��D�r�D��D�i�D��pDzh|Dq�Dg�ND^"�DT��DJڋDAZD7:	D-H�D#@�D#8D�fD�dC�Cߝ�Cʅ,C�@C�ξC�1ChΠC<�OC�hB�  
#float;1;131;ANGF;8
s-1
Angular Frequency
1.0
XVEC1
fcasson
PRFL
AF
723
GCP GCN�GCB�GC*�GC5GB��GB��GBU�GB}GA��GA<�G@�jG@I3G?�*G?*G>�hG=�G=.�G<q�G;�#G:�KG:rG9 �G86�G7D�G6J�G5H�G4?�G3/�G2,G0�G/��G.��G-�uG,M�G+FG)�%G(��G'QMG&*G$��G#k�G"EG �Gl�G�G�dGZRG��G�ZG;�G��Gy+GOG�^GS�G�G��G0�G
��G	rWG�G�G]�G&G�ZGVIG F�_�F��F�"�F��F���F�ibF���F�[)F�۫F�a`F��hF�|�F��FܮFF�OgF��8Fբ�F�U%F�UF��`F̏QF�Y,F�(�F���F��XF���F���F��
F��}F�}�F�} F��GF��LF��$F���F��;F��eF�IF�H�F�|F���F��F�6�F��5F���F�"�F�|RF���F�>�F���F��F��"F�RF�~vF� �F��cF�F��zF�8�F��LF�p�F�eF���
#float;1;131;PRAD;8
W m-3
Radiation
0.1
XVEC1
bolom
BARA
AVFL
422
HCP HDHE�&HG@HH�ZHJsHK��HM�HN��HP!�HQ��HS&HT�(HV*AHW�ZHY.uHZ��H\2�H]��H_6�H`��Hb;Hc�(He?AHf�\HhCuHiŎHkG�Hl��HnK�Ho��HqPHr�*HtTCHu�]HwXwHxڐHz\�H{��H}`�H~��H�2�H��H���H�u�H�6�H���H���H�y�H�:�H���H��H�~H�?"H� /H��<H��IH�CUH�cH��pH��|H�G�H��H�ɢH���H�K�H��H���H���H�O�H��H��
H��H�T#H�0H��>H��JH�XWH�cH��qH��}H�\�H��H�ޤH���H�`�H�!�H���H���H�d�H�%�H��H��H�i%H�*2H��>H��KH�mXH�.dH��rH��~H�q�H�2�H��H���H�u�H�6�H���H���H�y�H�:�H��H��H�~%H�?2H� ?H��KH��YH�CfH�rH��H���H�G�H��H�ɳH���H�K�H��H���H�H�P 
#float;1;131;ZEFF;8

Z-effective
1.0
XVEC1
JETPPF
KS3
ZEFH
287
?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y?�Y
#float;1;131;QNBE;8
W m-3
Power Density Electrons
0.1
XVEC1
JETPPF
NBIP
GE
321
It$ It"$It?Is�~Is��Is��IsC�Ir��Ir��Ir�Iq�#Ip��IpZ�Io��In��In,�ImX�Ilx�Ik��Ij�xIi�hIh�Ige�If@�Ie�Ic��Ib��IaJ�I_��I^��I]5:I[��IZUIIX��IWY�IU��ITFIR��IQ�IO��IM�IL<�IJ��IH�IG<!IE��IC��IB$I@m�I>��I<��I;B�I9�rI7ͬI6�I4XI2��I0�I/+I-sI+�_I* I(S1I&�I$��I#B�I!��I��IE�I�I��I_�I�?I)�I�_I Ip I�+IY�IӍIP�I	ѭIV I��Ii^I�xI�8I!�H�w}H��H��H�@�H��zH���H�L�H��H�$}H盋H��H��H�,�H��aH�]H� H֪!H�[fH��H��&H͙�H�f�H�;H�'H���H���H�ϰH��uH���H�ĄH�ͨH��&H���H��H�0�H�YH��,H��:H��$H�4�H�zDH��XH� 
#float;1;131;QNBI;8
W m-3
Power Density Ions
0.1
XVEC1
JETPPF
NBIP
GI
321
I�$ I�"$I�>I��wI���I�I�CsI���I�I�VI��I��MI�ZI�>I��!I�+�I�X I�w�I��I�>I�I�~�I�d0I�?5I��I���I�MI�H�I��Iޗ/I�2JI���I�Q�I��UI�V2I���I�A�Iү�I�)I�|I���I�7�Iʏ�I��wI�6aIŅ�I�ҮI��I�f�I���I���I�;�I���I���I�
�I�O�I��YI��cI�"7I�jI��I���I�I{I��,I��I�8ZI��I��4I�:�I���I��I�TI��iI��I�� I��I�cZI��SI�L�I��QI�CqI��I�H0I���I�[.I��I�|�I��IY�I|��IyגIw!�ItsAIq�AIo,�Il��Ij�Igz�Id��Ib~DI`I]�;I[:�IX�CIV�IT8IQ�IO�7IMuTIKB`IIRIF�IDҥIB��I@��I>�VI<�`I:��I8��I6�I4˄I2�7I1	I/0�I-^�I+��I)�[I(�I&Q	I$��I"�V
#float;1;131;SB1;8
m-3 s-1
Particle Source 1
1000000.0
XVEC1
JETPPF
NBIP
S0
321
U���U���U�{iU�j�U�QGU�/�U��U��zU��U�V�U�U���U�_eU��U��U�#;U��&U�+�U���U�U���U���U�K�U���U��U�H�U���U��U�!U�OzU��IU���U��&U��U�3qU�U�U~�U}!�U{T9Uy�\Uw��Uu�lUs�#Ur	(Up!�Un7�UlJ�Uj[�Uhj}Ufw�Ud��Ub�U`�sU^��U\�xUZ��UX�%UV�aUTكUR��UP�iUO�UM�UK+yUIA�UGY�UEt�UC�UA�:U?�IU=�^U<$�U:QU8��U6�,U4��U3%sU1c�U/��U-�EU,4�U*��U(�U')�U%�rU#�AU"C4U �NU�U�U��Uk�U�UfU�UqdU�]U��U!�U�mUWU��U
��U	E�U�
U�5UYdU�U϶U��U U�T�=9T�֙T�w�T� T��T�zT�F0T�$T��JT魆T��T�j�T�TT�C�T�:�T�7�T�;�T�E�T�V�T�m�
#float;1;131;JZNB;8
A m-2
NB Driven Curr.Dens.
1.0E7
XVEC1
JETPPF
NBIP
JBDC
321
<#�
<#�<#��<#)�<"��<!�< ��<Q9<ة<.�<T�<L~<P<�'<4E<�<��<̀<�l<�e;��M;�
�;�<V;�M�;�Ez;�(I;���;��O;Ɖ�;�N�;�o;��;���;��6;�ɑ;��;�,�;��^;�;uu ;i!;]�;Q_;F	�;;�;0yJ;&B%;mF;�V;	�;@�:��9:��:�z:���:�*�:�J�:��:��L:���:{":g�:U��:E	�:5A�:&��:��:��: K9��9զ�9¹�9�=�9��9�B9���9p9Yh9DJ90�98�94�9 ��8�ƛ8ν�8���8�B�8�v	8�g�8i��8O�=88~�8#��8Į7��x7��F7�B97�u 7�KC7���7m�o7P0�76x7�7
ʡ6��-6�oI6���6���6���6n�6MŰ61�j6 �6�D5��5¨5���5���5t��5P�52$#5�+5H4�]r4� L4���4�v�4a�4>c�4 yt
#float;1;131;TORQ;8
N m-2
Torque
0.1
XVEC1
JETPPF
NBIP
TORP
321
@   ?��?��m?��5?��s?�b3?��?���?�K�?��j?�F?���?��?�Q�?���?���?��{?��?���?��??��i?���?�?�nn?�0|?��1?��?�:?��?�g3?���?�r�?���?�_�?��?�1�?ޑs?��?�@X?ِJ?�۹?�#?�f�?Ҧ�?��?��?�U�?ˋ�?ɿ�?��"?�#y?�S�?�?��@?��?�-?�B?�r�?��?�֌?�
A?�?j?�v4?���?��^?�&?�e
?��o?��a?�1?�zk?�ƽ?�?�h�?��$?�?�sR?��?�6/?���?�??�u<?���?�\V?�Շ?�R�?��U?�W�?���?�l�?��=?��q?�)�?}�?z��?x�?ub?r�5?p&?m~�?j�.?hc3?e��?cf?`��?^�?\"�?Y��?WpK?U!�?R��?P�?Nb:?L0w?J�?G��?E��?C��?A� ??�?=��?;��?9��?7��?5��?3�A?2�?05�?.eB?,��?*�\
#float;1;131;QRFE;8
W m-3
Power Density Electrons
0.1
XVEC1
JETPPF
PION
PDCE...
278
JC�7JG�JM��JTR\JZ�JaN�Jg�{Jm��Js��Jy�fJ]J�f�J� /J�y!J��J���J�	-J��RJ��`J�&�J���J��J���J�lvJ��J�c�J��\J��/J�TMJ���J�H�J�v�J�sAJ�??J���J�JJ���J��#J���J�ZaJ��DJ�}�J�ݱJ~?�Jx��Jr��Jl�#Jfa2J`JY��JS �JL_!JE��J>�HJ8:�J1J*�>J$�Jz�J��Jq�J
)J��I�DI�6EI�oTI��I�ƟI���I�fTI�8I�b�I��I�ɷI�IA�Io-{I_КIQ)UIC5`I5��I)[�IoI'�I�!H��cH��H�C�Hř�H���H�d�H��H��H�9�HlvDHX�HE'XH3�BH#��H��H�lG�G�cfG�'G���G��eG��mG���GmlGT��G>lG*WWG-GďF���F�G�F�N$F�ƛF�x9F�.�FkseFO�=F7=BF!TkF�E��E�v�E�S�E�VjE�)�E~�
#float;1;131;QRFI;8
W m-3
Power Density Ions
0.1
XVEC1
JETPPF
PION
PDCI...
278
JC�7JG�JM��JTR\JZ�JaN�Jg�{Jm��Js��Jy�fJ]J�f�J� /J�y!J��J���J�	-J��RJ��`J�&�J���J��J���J�lvJ��J�c�J��\J��/J�TMJ���J�H�J�v�J�sAJ�??J���J�JJ���J��#J���J�ZaJ��DJ�}�J�ݱJ~?�Jx��Jr��Jl�#Jfa2J`JY��JS �JL_!JE��J>�HJ8:�J1J*�>J$�Jz�J��Jq�J
)J��I�DI�6EI�oTI��I�ƟI���I�fTI�8I�b�I��I�ɷI�IA�Io-{I_КIQ)UIC5`I5��I)[�IoI'�I�!H��cH��H�C�Hř�H���H�d�H��H��H�9�HlvDHX�HE'XH3�BH#��H��H�lG�G�cfG�'G���G��eG��mG���GmlGT��G>lG*WWG-GďF���F�G�F�N$F�ƛF�x9F�.�FkseFO�=F7=BF!TkF�E��E�v�E�S�E�VjE�)�E~�
*
*EOF
